package main

import (
	"fmt"
	"net/http"

	"gitlab.com/jaime/go-handlers/pkg/user"
)

func main() {
	userHandler := user.New(&s{})

	m := http.NewServeMux()
	m.HandleFunc("/user", userHandler.CreateUser)

	server := &http.Server{
		Addr:    ":9999",
		Handler: m,
	}
	fmt.Println("server listening on :9999")
	if err := server.ListenAndServe(); err != nil {
		fmt.Printf("failed to start server: %+v", err)
	}
}

type s struct{}

func (s *s) Create(user *user.User) error {
	if user.Name == "" {
		return fmt.Errorf("fail on purpose")
	}

	return nil
}
