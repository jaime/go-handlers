package main

import (
	"fmt"
	"net/http"

	"gitlab.com/jaime/go-handlers/pkg/user"
)

func main() {
	userHandler := user.New(&s{})

	m := http.NewServeMux()
	m.HandleFunc("/user", userHandler.CreateUser)

	server := &http.Server{
		Addr:    ":8888",
		Handler: m,
	}
	fmt.Println("server listening on :8888")
	if err := server.ListenAndServe(); err != nil {
		fmt.Printf("failed to start server: %+v", err)
	}
}

type s struct{}

func (s *s) Create(user *user.User) error {
	if user.ID == 0 {
		return fmt.Errorf("fail on purpose")
	}

	return nil
}
