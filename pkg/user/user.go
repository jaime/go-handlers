package user

import (
	"encoding/json"
	"net/http"
)

type User struct {
	ID   int64
	Name string
	Age  int64
}

type Storage interface {
	Create(user *User) error
}

type Handler struct {
	store Storage
}

func New(store Storage) *Handler {
	return &Handler{
		store: store,
	}
}

func (uh *Handler) CreateUser(w http.ResponseWriter, r *http.Request) {
	// TODO: add your validations here
	// TODO: add tests mocking handler
	// TODO: DRY a lot, especially your handlers on error handling

	newUser := User{}
	if err := json.NewDecoder(r.Body).Decode(&newUser); err != nil {
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	if err := uh.store.Create(&newUser); err != nil {
		http.Error(w, "something went wrong", http.StatusInternalServerError)
		return
	}

	if err := json.NewEncoder(w).Encode(newUser); err != nil {
		http.Error(w, "something went wrong", http.StatusInternalServerError)
		return
	}
}
